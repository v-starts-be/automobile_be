const express = require("express");
const router = express.Router();

router.get("/services/:type", (req, res) => {
  if (req.params.type === "car") {
    res.send(
      '[\r\n  {\r\n    "id": "1",\r\n    "name": "Periodic Services",\r\n    "imageUrl": "https://image.shutterstock.com/image-photo/close-hands-unrecognizable-mechanic-doing-260nw-744678172.jpg",\r\n    "serviceType": "Basic"\r\n  },\r\n  {\r\n    "id": "2",\r\n    "name": "AC Services",\r\n    "imageUrl": "https://images.pexels.com/photos/2244746/pexels-photo-2244746.jpeg?cs=srgb&dl=pexels-malte-luk-2244746.jpg&fm=jpg",\r\n    "serviceType": "Basic"\r\n  },\r\n  {\r\n    "id": "3",\r\n    "name": "Cleaning & Detailing Check",\r\n    "imageUrl": "https://images.pexels.com/photos/4870700/pexels-photo-4870700.jpeg?cs=srgb&dl=pexels-karolina-grabowska-4870700.jpg&fm=jpg",\r\n    "serviceType": "Basic"\r\n  },\r\n  {\r\n    "id": "4",\r\n    "name": "Tyres & Wheel core",\r\n    "imageUrl": "https://images.pexels.com/photos/3807695/pexels-photo-3807695.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500",\r\n    "serviceType": "Basic"\r\n  },\r\n  {\r\n    "id": "5",\r\n    "name": "Batteries",\r\n    "imageUrl": "https://images.pexels.com/photos/4517073/pexels-photo-4517073.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500",\r\n    "serviceType": "Basic"\r\n  },\r\n  {\r\n    "id": "6",\r\n    "name": "Light & Fitments",\r\n    "imageUrl": "https://images.pexels.com/photos/4488667/pexels-photo-4488667.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500",\r\n    "serviceType": "Basic"\r\n  },\r\n  {\r\n    "id": "6",\r\n    "name": "Denting Painting",\r\n    "imageUrl": "https://image.shutterstock.com/image-photo/worker-painting-car-paint-booth-260nw-103377908.jpg",\r\n    "serviceType": "Basic"\r\n  }\r\n]'
    );
  } else {
    res.send(
      '[\r\n  {\r\n    "id": "1",\r\n    "name": "Periodic Services",\r\n    "imageUrl": "https://images.pexels.com/photos/3822843/pexels-photo-3822843.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500",\r\n    "serviceType": "Basic"\r\n  },\r\n  {\r\n    "id": "2",\r\n    "name": "Cleaning",\r\n    "imageUrl": "https://images.pexels.com/photos/4876631/pexels-photo-4876631.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",\r\n    "serviceType": "Basic"\r\n  },\r\n  {\r\n    "id": "3",\r\n    "name": "Tyres & Wheel core",\r\n    "imageUrl": "https://images.pexels.com/photos/3817784/pexels-photo-3817784.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",\r\n    "serviceType": "Basic"\r\n  },\r\n  {\r\n    "id": "4",\r\n    "name": "Batteries",\r\n    "imageUrl": "https://images.pexels.com/photos/4517073/pexels-photo-4517073.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500",\r\n    "serviceType": "Basic"\r\n  },\r\n  {\r\n    "id": "5",\r\n    "name": "Light & Fitments",\r\n    "imageUrl": "https://images.pexels.com/photos/4488667/pexels-photo-4488667.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500",\r\n    "serviceType": "Basic"\r\n  },\r\n  {\r\n    "id": "5",\r\n    "name": "Denting Painting",\r\n    "imageUrl": "https://5.imimg.com/data5/WU/RL/MY-56768733/bike-painting-services-500x500.jpg",\r\n    "serviceType": "Basic"\r\n  }\r\n]'
    );
  }
});

module.exports = router;
