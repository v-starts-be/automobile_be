const express = require("express");
const app = express();

const routes = require("./Routes/index.js");
app.use("/", routes);

app.listen(3000, () => {
  console.log("listening to 3000");
});
